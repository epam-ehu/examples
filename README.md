# examples

## test-02-task-examples

Создать класс University с полями:
```text
title: string
age: number
location: string
people: Array<People>
getTeachers: возвращает список преподавателей
findStudentById: возвращает студента если он есть по его айди
getPeople: возвращает просто список людей из университета 
```

Создать класс People
```text
id: number - автоматически генерируется при создании экземпляра класса
name: string
```

Создать класс Student который наследует People
```text
marks: Array<number>
```

Создать класс Teacher который наследует People
```text
salary: number
```
