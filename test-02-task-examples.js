/**
 * Определяет является ли значение переменной строкой
 * @param value
 * @returns {boolean}
 */
function isString(value) {
    return typeof value === "string";
}

/**
 * Определяет является ли значение переменной числом
 * @param value
 * @returns {boolean}
 */
function isNumber(value) {
    return Number.isInteger(value);
}

/**
 * Генерирует случайную уникальную строку
 * @returns {string}
 */
function generateId() {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
}

class University {
    #title; // значение по умолчанию - undefined
    #age;
    #location = ''; // задаем значение по умолчанию равное пустой строке ''
    #people;

    constructor(title, age, location) {
        this.title = title; // здесь намеренно идет присвоение через setter функцию (для проверки входных данных)
        this.#age = age; // здесь можно присвоить сразу без проверок на тип входных данных
        this.#location = location;

        this.#people = []; // задаем значение по умолчанию в функции-конструктор
    }

    /**
     * Используется для задания нового значения внутренней переменной #title
     * Вызывается через this.title = 'чт-то' если вызов происходит внутри класса
     * Вызывается через universityInstance.title = 'что-то' если вызов происходит из-вне класса (например, после const universityInstance = new University(...)
     * @param newTitle
     */
    set title(newTitle) {
        if (typeof newTitle !== "string") {
            throw new Error("Title must be a string");
        }

        this.#title = newTitle;
    }

    /**
     * Используется для получения значения #title из-вне либо внутри класса
     * Вызывается через this.title если вызов происходит внутри класса
     * Вызывается через universityInstance.title если вызов происходит из-вне класса (например, после const universityInstance = new University(...)
     * @returns {string}
     */
    get title() {
        return this.#title;
    }

    addPeople(people) {
        if (people instanceof People) {
            this.#people.push(people);
        } else {
            throw new Error('Only instanceof People are allowed to be added');
        }
    }

    /**
     * Ищет студента по его уникальному номеру id с задержкой в 0.5 секунды
     * @param id
     * @returns {Promise<unknown>}
     */
    findStudentById(id) {
        // возвращаем экземепляр класса Promise из функции
        // стрелочные функции используются чтобы не потерять контекст this когда вызываем this.#people.find...
        return new Promise((resolve, reject) => {
            // начинается создание асинхронного действия по поиску студента по его id посредством вызова setTimeout(...)
            setTimeout(() => {
                // коллбэк-функция будет вызвана когда таймаут закончится

                // ищем студента в массиве #people
                const student = this.#people.find((item) => {
                    // функция-предикат вызывается для каждого элемента item массива #people

                    // так как в массиве #people у нас экземпляры разных классов, то нужно проверить, что только Student мы должны найти
                    const isItemStudent = item instanceof Student;

                    if (isItemStudent) {
                        // если все ок и item это Student, то функция-предикат возвращает результат сравнение id и item.id
                        return item.id === id;
                    } else {
                        // если item - это какой-то экземпляр другого класса
                        return false;
                    }
                });

                if (student) {
                    // если какой-либо студент найден, то возвращаем его из Promise
                    resolve(student);
                } else {
                    // если никого не нашли, то завершаем Promise с результатом не найдено
                    reject(`Cannot find student with id=${id}`);
                }
            }, 500);
        });
    }

    /**
     * Возвращает список учителей с задержкой в 1 секунду
     * @returns {Promise<Array>}
     */
    getTeachers() {
        // возвращаем экземепляр класса Promise из функции
        // Чтобы не потерять контекст this когда вызываем this.#people.forEach...
        // оформляем функцию, которую принимает new Promise(), не как стрелочную, а как обычную
        // и привязываем к ней контекст через bind(this)
        const promiseExecutorFn = function (resolve, reject) {
            setTimeout(() => {
                const teachers = [];

                this.#people.forEach(item => {
                    if (item instanceof Teacher) {
                        teachers.push(item);
                    }
                });

                resolve(teachers);
            }, 1000);
        }

        return new Promise(promiseExecutorFn.bind(this));
    }

    /**
     * Возвращает список людей (как студентов, так и учителей)
     * @param {number} amount количество, которое нужно вернуть
     * @param {number} start порядковый номер (не id) человека, начиная с которого вернуть amount людей
     * @returns {*}
     */
    getPeople(amount = 3, start = 0) {
        const people = this.#people.slice(start, amount);

        return people;
    }
}

class People {
    #id;
    #name;

    constructor(name) {
        if (isString(name)) {
            this.#id = generateId(); // каждый раз задаем случайную строку как id
            this.#name = name;
        } else {
            throw new Error("Cannot create People, name is not a string");
        }
    }

    get id() {
        return this.#id;
    }

    set name(name) {
        if (isString(name)) {
            this.#name = name;
        } else {
            throw new Error("Cannot set name, must be a string");
        }
    }

    get name() {
        return this.#name;
    }
}

class Student extends People {
    #marks = [];

    constructor(name) {
        super(name); // так как мы наследовались от класса People, то вызываем конструкторв People через функци super
    }
}

class Teacher extends People {
    #salary;

    constructor(name, salary) {
        super(name);

        this.salary = salary;
    }

    set salary(num) {
        this.#salary = num;
    }

    get salary() {
        return this.#salary;
    }
}

// создаем экземепляр класса University
const university = new University('EHU', 50, 'Vilnius');

// создаем массив студентов
const studentsDatabase = [
    new Student('Ivan Frolov'),
    new Student('Mary Jane'),
    new Student('Jack Jones'),
    new Student('Nastya Kirbie'),
    new Student('Gilbert Arenas'),
];

// создаем массив учителей
const teachersDatabase = [
    new Teacher('Ms Jane', 500),
    new Teacher('Mr Cat', 1500),
]

// добавляем всех студентов в университет
studentsDatabase.forEach(student => {
    university.addPeople(student);
});

// добавляем всех учителей в университет
teachersDatabase.forEach(teacher => {
    university.addPeople(teacher);
});

// вывод для проверки - все люди буду в университете
console.log(university);

// проверка и отлов ошибок если пытаемся добавить не человека в университет
try {
    // вызываем функцию addPeople и передаем обычный объект
    university.addPeople({ name: 'Bug' });
} catch (error) {
    // ловим ошибку и выводим сообщения (
    console.error(error);
}

// тестируем функцию поиска студента по его id

// успешный случай
university
    .findStudentById(
        studentsDatabase[2].id // ищем студента под индексом 2 из тестовой базы данных
    )
    .then(
        (student) => {
            console.log(student); // успешно нашли студента, обработали и вывели
        }
    )

// случай когда студент не найден
university
    .findStudentById(
        'sdf3o4f433934034' // ищем студента с несуществующим id
    )
    .then(
        (student) => {
            console.log(student); // успешно нашли студента, обработали и вывели
        }
    )
    .catch(
        (error) => {
            console.log(error); // студент не найден, вывели сообщение об ошибке
        }
    )

// тестируем возврат учителей из университета
university
    .getTeachers()
    .then(
        function (teachers) {
            if (teachers.length > 0) {
                console.log(teachers);
            } else {
                console.log('There are no any teachers in university');
            }
        }
    )

// тестируем возврат людей из университета
const peopleFromUniversity = university.getPeople();
peopleFromUniversity.forEach(item => {
    console.log(item.name); // у каждого человека есть имя, выводим его
});
